module.exports = function(config) {
  config.set({
    basePath: './',
    frameworks: ['browserify', 'jasmine'],
    files: ['app/**/*.spec.js'],
    preprocessors: {
      'app/**/*.spec.js': ['browserify']
    },
    browserify: {
      debug: true,
      paths: ['app'],
      transform: ['stringify', 'lessify', 'babelify', 'deamdify']
    },
    coverageReporter: {
      reporters: [{type: 'html'}, {type: 'text'}]
    },
    reporters: ['mocha', 'coverage'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: false,
    browsers: ['Chrome'],
    singleRun: true
  });
};
