import 'babel-polyfill';

import angular from 'angular';

import app from 'app';
import dev from 'app.dev';

angular.element(document).ready(() => {
  angular.bootstrap(document, [process.env.NODE_ENV === 'dev' ? dev : app]);
});
