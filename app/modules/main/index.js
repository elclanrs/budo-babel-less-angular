import angular from 'angular';
import uiRouter from 'angular-ui-router';

import mainComponent from 'modules/main/components/main';

angular.module('app.main', [
  uiRouter
])

.config(($stateProvider, $urlRouterProvider) => {
  'ngInject';

  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('main', {
      url: '/',
      template: '<my-main><my-main>'
    });
})

.component('myMain', mainComponent)
;

export default 'app.main';
