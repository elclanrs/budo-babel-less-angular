import angular from 'angular';

import 'app.less';

import uiRouter from 'angular-ui-router';
import uiBootstrap from 'angular-ui-bootstrap';
import main from 'modules/main';

angular.module('app', [
  uiRouter,
  uiBootstrap,
  main
])

.run(($rootScope) => {
  'ngInject';

  $rootScope.ENV = process.env.NODE_ENV;
})
;

export default 'app';
