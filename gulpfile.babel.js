import 'babel-polyfill';
import gulp from 'gulp';
import budo from 'budo';
import gulpif from 'gulp-if';
import rename from 'gulp-rename';
import source from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';
import flatten from 'gulp-flatten';
import browserify from 'browserify';
import uglify from 'gulp-uglify';
import ngAnnotate from 'gulp-ng-annotate';
import Autoprefix from 'less-plugin-autoprefix';
import ejs from 'gulp-ejs';
import size from 'gulp-check-filesize';
import karma from 'karma';
import packageJson from './package.json';
import yargs from 'yargs';

const ENV_DEV = 'dev';
const ENV_PROD = 'prod';

const INPUT_PATH = 'app';
const INPUT_NAME = 'bootstrap.js';
const OUTPUT_PATH = 'dist';
const OUTPUT_NAME = 'bundle.js';
const OUTPUT_VENDOR_NAME = 'vendor.js';

let argv = yargs.default('env', ENV_DEV).argv;

function vendorDependencies(env) {
  if (env === ENV_DEV) {
    return [];
  }
  return Object.keys(packageJson.dependencies).filter(dep => {
    let exclude = [
      'font-awesome',
      'bootstrap'
    ];
    return !exclude.find(d => d === dep);
  });
}

function browserifyConfig(env) {
  return {
    paths: [INPUT_PATH],
    transform: [
      ['eslintify', {continuous: env === ENV_DEV}],
      'stringify',
      ['lessify', {rootpath: OUTPUT_PATH, plugins: [new Autoprefix({browsers: 'last 2 versions'})]}],
      'babelify',
      'deamdify',
      ['envify', {_: 'purge', global: true, NODE_ENV: env}]
    ]
  };
}

gulp.task('copy:fonts', () => {
  return gulp.src([
    'node_modules/font-awesome/**/*.{otf,svg,ttf,woff,woff2}',
    'node_modules/bootstrap/**/*.{otf,svg,ttf,woff,woff2}'
  ])
  .pipe(flatten())
  .pipe(gulp.dest(`${OUTPUT_PATH}/fonts`));
});

gulp.task('copy:html', () => {
  return gulp.src(`${INPUT_PATH}/index.ejs`)
    .pipe(ejs({
      vendor: OUTPUT_VENDOR_NAME,
      bundle: OUTPUT_NAME
    }))
    .pipe(rename('index.html'))
    .pipe(gulp.dest(OUTPUT_PATH));
});

gulp.task('vendor', () => {
  return browserify()
    .require(vendorDependencies(argv.env))
    .bundle()
    .pipe(source(OUTPUT_VENDOR_NAME))
    .pipe(buffer())
    .pipe(uglify({preserveComments: 'license'}))
    .pipe(gulp.dest(OUTPUT_PATH));
});

gulp.task('build', ['test', 'vendor', 'copy:html', 'copy:fonts'], () => {
  return browserify(`${INPUT_PATH}/${INPUT_NAME}`, browserifyConfig(argv.env))
    .external(vendorDependencies(argv.env))
    .bundle()
    .pipe(source(OUTPUT_NAME))
    .pipe(buffer())
    .pipe(gulpif(argv.env === ENV_PROD, ngAnnotate()))
    .pipe(gulpif(argv.env === ENV_PROD, uglify({preserveComments: 'license'})))
    .pipe(gulp.dest(OUTPUT_PATH))
    .on('end', () => {
      gulp.src(`${OUTPUT_PATH}/*.js`).pipe(size({enableGzip: true}));
    });
});

gulp.task('serve', ['vendor', 'copy:html', 'copy:fonts'], () => {
  budo(`${INPUT_PATH}/${INPUT_NAME}`, {
    live: true,
    port: 8000,
    dir: OUTPUT_PATH,
    serve: OUTPUT_NAME,
    browserify: browserifyConfig(ENV_DEV)
  });
});

gulp.task('test', (done) => {
  new karma.Server({
    configFile: `${__dirname}/karma.conf.js`,
    singleRun: true
  }, (status) => {
    if (status) {
      return process.exit();
    }
    done();
  }).start();
});

gulp.task('default', ['serve']);
