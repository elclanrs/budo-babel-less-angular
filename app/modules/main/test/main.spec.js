import angular from 'angular';
import 'angular-mocks';
import 'modules/main';

describe('Main', () => {
  let $rootScope;

  beforeEach(angular.mock.module('app.main'));

  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
  }));

  it('$rootScope exists', () => {
    expect($rootScope).toBeDefined();
  });
});
