## Instructions

Requirements:

- Node 4.4.0+ LTS

```sh
# Install dependencies
$ npm install

# Serve app for development with livereload on port 8000
$ gulp

# Build app for development
$ gulp build

# Build app for production
$ gulp build --env=prod

# Test
$ gulp test
```

## Why npm and Browserify?

### 1) Bower may be dying

- Bower has 50K+ packages vs npm's 250K+
- Latest version of lodash [dropped Bower support](https://github.com/lodash/lodash/wiki/Changelog#jan-12-2016--diff--docs)
- [Bower may be dying!](https://github.com/aspnet/Home/issues/1074)


### 2) Current workflow is too laborious

Importing a module with Bower and RequireJS:

```sh
$ bower install --save my-module

// config.js
require.config({
  shim: {
    myModule: ['dependency']
  },
  paths: {
    myModule: 'path/to/my/module.js'
  }
});

// main.js
(function(define) {
  define(['myModule'], function(myModule) {
   // use myModule
  });
}(define));
```

Importing a module with npm and Browserify:

```sh
$ npm install --save my-module

// main.js
import myModule from 'my-module';
```

### 3) Other

- **AMD:** Browserify has a compatibility layer for AMD modules (modules defined with RequireJS) so the integration is seamless: `import myAmdModule from 'my-amd-module'`. It can also export AMD modules.

- **Shim:** Browserify can shim libraries that are not using modules (ie. globals). It can import modules installed with Bower as well.

- **Portability:** Browserify can create self-contained components. It can bundle a module in a single minified JavaScript file, including HTML and CSS.

- **ES2015:** With Browserify and [Babel](https://babeljs.io/) we can start using all the [new JavaScript features](https://babeljs.io/docs/learn-es2015/) without fearing browser compatibility issues.

- **Shrinkwrap:** With npm we can lock down the dependency tree so we can ensure our deployment builds are reproducible without having to push the vendor folder into the repo.
