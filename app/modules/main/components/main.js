import 'modules/main/styles/main.less';

import template from 'modules/main/templates/main.html';
import controller from 'modules/main/controllers/main';

export default {
  template,
  controller
};
