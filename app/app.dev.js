import angular from 'angular';

import app from 'app';
import ngMockE2E from 'angular-mocks/ngMockE2E';

angular.module('app.dev', [
  app,
  ngMockE2E
])

.run(($rootScope, $httpBackend) => {
  'ngInject';

  $httpBackend.whenGET(/.*/).passThrough();
})
;

export default 'app.dev';
